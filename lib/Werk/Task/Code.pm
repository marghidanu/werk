package Werk::Task::Code {
	use Moose;

	extends 'Werk::Task';

	has 'code' => (
		is => 'ro',
		isa => 'CodeRef',
		required => 1,
	);

	sub run {
		my ( $self, $context ) = @_;

		return $self->code()->( $self, $context );
	}

	__PACKAGE__->meta()->make_immutable();
}

1;

__END__

=head1 NAME

Werk::Task::Code

=head1 DESCRIPTION

=head1 ATTRIBUTES

=head2 code

=head1 METHODS

=head2 run

=head1 AUTHOR

Tudor Marghidanu <tudor@marghidanu.com>
