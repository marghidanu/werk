package Werk::Task::Dumper {
	use Moose;

	extends 'Werk::Task';

	use Data::Dumper;

	sub run {
		my ( $self, $context ) = @_;

		return print( Dumper( $context ) );
	}

	__PACKAGE__->meta()->make_immutable();
}

1;

__END__

=head1 NAME

Werk::Task::Dumper

=head1 DESCRIPTION

=head1 METHODS

=head2 run

=head1 AUTHOR

Tudor Marghidanu <tudor@marghidanu.com>
