#!/usr/bin/env perl

use strict;
use warnings;

use Test::More;
use Test::Perl::Critic (
	-severity => 1,
	-exclude => [
		'strict',
		'warnings',
		'constant',
		'ProhibitBooleanGrep',
		'ProhibitParensWithBuiltins',
		'RequireTidyCode',
		'RequireVersionVar',
	],
);

all_critic_ok();
